import {
    StyleSheet,
    Dimensions,
} from 'react-native';

export const deviceHeight = Dimensions.get('window').height;
export const deviceWidth = Dimensions.get('window').width;

export default styles = StyleSheet.create({

    container: {
       backgroundColor:"white",
       flex:1,
       flexDirection:"row",
       height:50,
       width:deviceWidth,
       borderBottomColor:'#E0E0E0',
       borderBottomWidth:1,
    },
    textStyle:{
      fontSize:13,
      paddingTop:18,
      paddingRight:40,
      color:'#FF9800',
      justifyContent:'center',
    },
    NtextStyle:{
      fontSize:13,
      paddingTop:7,
      paddingRight:40,
      color:'#FF9800',
      justifyContent:'center',
    },
    ConfirmationTextStyle:{
      fontSize:13,
      paddingTop:18,
      paddingRight:100,
      color:'#FF9800',
      justifyContent:'center',
    },
    ActuelTextStyle:{
      fontSize:13,
      paddingTop:18,
      paddingRight:60,
      color:'#FF9800',
      justifyContent:'center',
    },

});
