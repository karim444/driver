import React, { Component } from "react";
import moment from "moment";
import { connect } from "react-redux";
import _ from "lodash";
import { View, Platform, Dimensions } from "react-native";
import FAIcon from "react-native-vector-icons/FontAwesome";
import PropTypes from "prop-types";
import {
  Container,
  Header,
  Content,
  Text,
  Button,
  Icon,
  Thumbnail,
  Card,
  CardItem,
  Title,
  Left,
  Right,
  Body
} from "native-base";
import { Actions } from "react-native-router-flux";

import { fetchTripHistoryAsync } from "../../../actions/driver/history";
import styles from "./styles";
import commonColor from "../../../../native-base-theme/variables/commonColor";

const { width } = Dimensions.get("window");

function mapStateToProps(state) {
  return {
    jwtAccessToken: state.driver.appState.jwtAccessToken,
    trips: state.driver.history.trips,
  };
}

class History extends Component {
  static propTypes = {
    jwtAccessToken: PropTypes.string,
    trips: PropTypes.array,
    fetchTripHistoryAsync: PropTypes.func,
  };

  async componentDidMount() {
    await this.props.fetchTripHistoryAsync(this.props.jwtAccessToken);
  }

  formatDate(bookingTime) {
    // eslint-disable-line class-methods-use-this
    return moment(bookingTime).format(" ddd Do MMM YYYY h:mm a");
  }

  render() {
    return (
      <Container style={{ backgroundColor: "#fff" }}>
        <Header
          androidStatusBarColor={commonColor.statusBarLight}
          style={Platform.OS === "ios" ? styles.iosHeader : styles.aHeader}
        >
          <Left>
            <Button transparent onPress={() => Actions.pop()}>
              <Icon
                name="md-arrow-back"
                style={{ fontSize: 28, color: commonColor.brandPrimary }}
              />
            </Button>
          </Left>
          <Body>
            <Title
              style={
                Platform.OS === "ios"
                  ? styles.iosHeaderTitle
                  : styles.aHeaderTitle
              }
            >
              Trip History
            </Title>
          </Body>
          <Right />
        </Header>
      </Container>
    );
  }
}

function bindActions(dispatch) {
  return {
    fetchTripHistoryAsync: jwtAccessToken =>
      dispatch(fetchTripHistoryAsync(jwtAccessToken))
  };
}

export default connect(mapStateToProps, bindActions)(History);
