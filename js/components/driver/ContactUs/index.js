import React, { Component } from "react";
import moment from "moment";
import { connect } from "react-redux";
import _ from "lodash";
import { View, Platform, Dimensions, Image, TouchableOpacity } from "react-native";
import FAIcon from "react-native-vector-icons/FontAwesome";
import PropTypes from "prop-types";
import LinearGradient from 'react-native-linear-gradient';

import {
  Container,
  Header,
  Content,
  Text,
  Button,
  Icon,
  Thumbnail,
  Card,
  CardItem,
  Title,
  Left,
  Right,
  Body
} from "native-base";
import { Actions } from "react-native-router-flux";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

import { fetchTripHistoryAsync } from "../../../actions/driver/history";
import styles from "./styles";
import commonColor from "../../../../native-base-theme/variables/commonColor";

var logo= require("../../../../assets/images/LOGO-CHAUFFEUR.png");
var fb= require("../../../../assets/images/fb.png");
var twitter= require("../../../../assets/images/twitter.png");
var phone= require("../../../../assets/images/phone.png");
var email= require("../../../../assets/images/email.png");
var address= require("../../../../assets/images/address.png");
var linkedin= require("../../../../assets/images/linkedin.png");




const { width } = Dimensions.get("window");

function mapStateToProps(state) {
  return {
    jwtAccessToken: state.driver.appState.jwtAccessToken,
    trips: state.driver.history.trips,
  };
}

class ContactUs extends Component {
  static propTypes = {
    jwtAccessToken: PropTypes.string,
    trips: PropTypes.array,
    fetchTripContactUsAsync: PropTypes.func,
  };

  render() {
    return (
      <View style={{flex:1}}>
        <LinearGradient colors={["#F44336","#EF6C00","#FF9800", "#FFB74D"]}
          style={{flex: 1,
           height:45,
           width: deviceWidth}}>

           <View style={{flex:1,flexDirection:'row'}}>
              <Button
                style={{paddingLeft:5,
                paddingTop:12
                      }}

                transparent onPress={() => Actions.pop()}>
                <Icon
                  name="md-arrow-back"
                  style={{ fontSize: 28, color:"white" }}
                />
              </Button>
              <Title
                style={{ color:"white",
                   fontSize:16,
                   fontWeight:'lighter',
                   paddingLeft:80,
                   paddingTop:20
                  }}>
                CONTACT US
              </Title>
           </View>
         </LinearGradient>



           <View style={{flex:9,backgroundColor:"#EEEEEE"}}>
             <View>
             <Image source={logo} style={{height:90, width:200,resizeMode: 'contain',marginLeft:80}} />
             </View>
             <View style={{flexDirection:'row',justifyContent:'space-around',paddingTop:20}}>
               <Image source={fb}
                 style={{
                   height:50,
                    width:50,
                    borderRadius:25,
                    resizeMode: 'contain',
                    marginLeft:10,
                    paddingLeft:30}} />


                   <Text
                     style={{
                       color: "black",
                       fontSize: 15,
                       fontWeight: "bold",
                       paddingVertical: 2,
                       paddingTop:15
                     }}
                   >
                    FACEBOOK:
                   </Text>
                 <TouchableOpacity>
                   <Text
                     style={{
                       color: "black",
                       fontSize: 15,
                       fontWeight: "lighter",
                       paddingVertical: 2,
                       paddingTop:15

                     }}
                   >
                    facebook.com/Shipotsu
                   </Text>
                 </TouchableOpacity>
                 </View>
                         <View style={{flexDirection:'row',justifyContent:'space-around',paddingTop:20}}>
                           <Image source={phone}
                             style={{
                               height:50,
                                width:50,
                                borderRadius:25,
                                resizeMode: 'contain',
                                }} />


                               <Text
                                 style={{
                                   color: "black",
                                   fontSize: 15,
                                   fontWeight: "bold",
                                   paddingVertical: 2,
                                   paddingTop:15,
                                   paddingRight:30
                                 }}
                               >
                                PHONE:
                               </Text>
                             <TouchableOpacity>
                               <Text
                                 style={{
                                   color: "black",
                                   fontSize: 15,
                                   fontWeight: "lighter",
                                   paddingVertical: 2,
                                   paddingTop:15

                                 }}
                               >
                                +216 40 229 317
                               </Text>
                             </TouchableOpacity>
                             </View>


                             <View style={{paddingRight:25,flexDirection:'row',justifyContent:'space-around',paddingTop:20}}>
                               <Image source={email}
                                 style={{
                                   height:50,
                                    width:50,
                                    borderRadius:25,
                                    resizeMode: 'contain',
                                    marginLeft:10,
                                    paddingLeft:30}} />


                                   <Text
                                     style={{
                                       color: "black",
                                       fontSize: 15,
                                       fontWeight: "bold",
                                       paddingVertical: 2,
                                       paddingTop:15
                                     }}
                                   >
                                    E-MAIL:
                                   </Text>
                                 <TouchableOpacity>
                                   <Text
                                     style={{
                                       color: "black",
                                       fontSize: 15,
                                       fontWeight: "lighter",
                                       paddingVertical: 2,
                                       paddingTop:15

                                     }}
                                   >
                                    shipotsu@toolynk.com
                                   </Text>
                                 </TouchableOpacity>
                                 </View>




             </View>
             <LinearGradient colors={["#F44336","#EF6C00","#FF9800", "#FFB74D"]} style={{flex: 1,
              height:45,
              width: deviceWidth}}>

             <View style={{flex:1,flexDirection:'row',justifyContent:'center'}}>
               <Text
                 style={{
                   color: "white",
                   fontSize: 15,
                   fontWeight:'bold',
                   paddingVertical: 2,
                   paddingTop:15

                 }}
               >
                  Shipotsu: Développons la confiance </Text>
             </View>
           </LinearGradient>


    </View>
    );
  }
}

function bindActions(dispatch) {
  return {
    fetchTripContactUsAsync: jwtAccessToken =>
      dispatch(fetchTripContactUsAsync(jwtAccessToken))
  };
}

export default connect(mapStateToProps, bindActions)(ContactUs);
