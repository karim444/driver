import React, { Component } from "react";
import moment from "moment";
import { connect } from "react-redux";
import _ from "lodash";
import { View, Platform, Dimensions, Image, TouchableOpacity } from "react-native";
import FAIcon from "react-native-vector-icons/FontAwesome";
import PropTypes from "prop-types";
import LinearGradient from 'react-native-linear-gradient';


import {
  Container,
  Header,
  Content,
  Text,
  Button,
  Icon,
  Thumbnail,
  Card,
  CardItem,
  Title,
  Left,
  Right,
  Body
} from "native-base";
import { Actions } from "react-native-router-flux";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

import styles from "./styles";
import commonColor from "../../../../native-base-theme/variables/commonColor";

let profilePic=require('../../../../assets/images/avatar-client.png');
let profileLogo=require('../../../../assets/images/mon-profil-nameIco.png');
let mail=require('../../../../assets/images/msg.png');
let password=require('../../../../assets/images/lock.png');
let phone=require('../../../../assets/images/tell.png');
let pencil=require('../../../../assets/images/pencil.png');



const { width } = Dimensions.get("window");

function mapStateToProps(state) {
  return {
    jwtAccessToken: state.driver.appState.jwtAccessToken,
    trips: state.driver.history.trips,
    fname: state.driver.user.fname,
    lname: state.driver.user.lname,
  };
}

class profile extends Component {
  static propTypes = {
    jwtAccessToken: PropTypes.string,
  };

  render() {
    return (
      <View style={{flex:1}}>
        <LinearGradient
          start={{x: 1.0, y: 2.0}} end={{x: 0.2, y: 0.2}}
          colors={['#FF3D00', '#FF7043', '#FFB74D']}
          style={{flex: 1,
           height:35,
           width: deviceWidth}}>

           <View style={{flex:1,flexDirection:'row'}}>
              <Title
                style={{ color:"white",
                   fontSize:14,
                   fontWeight:'lighter',
                   paddingLeft:150,
                   paddingTop:15
                  }}>
                MON PROFIL
              </Title>
              <View style={{paddingTop:15,paddingLeft:35}}>
              <TouchableOpacity
                button
                onPress={() => {
                Actions.modification();
                         }}
              >
                  <Image
                    source={pencil}
                    style={{ color:"white",height:15,width:15,resizeMode:"contain",paddingTop:15, paddingLeft:140,paddingTop:10 }}
                    />
                </TouchableOpacity>
              </View>
           </View>
         </LinearGradient>






           <View style={{flex:12,backgroundColor:"#F5F5F5",flexDirection:'column'}}>
             <View style={{flex:2,backgroundColor:"#F5F5F5",alignItems:'center'}}>
              <View
                 style={{
                   paddingLeft:135,
                   paddingTop:30}}>
               <Image source={profilePic}
                 style={{
                   width: 110,
                   height: 110,
                   borderRadius: 55,
                   borderWidth: 0,
                   borderColor: "transparent",
                   marginRight:123,

                 }}
                 >
               </Image>
             </View>
               <Text
                   style={{
                     color:'black',
                     fontSize: 22,
                     paddingTop:15,
                     paddingLeft:20
                   }}
                 >
                 {_.get(this.props, "fname", "")}{" "}
                 {_.get(this.props, "lname", "")}
                 </Text>

                 <Text
                     style={{
                       color:'red',
                       fontSize: 18,
                       fontFamily: 'sans-serif-light',
                       paddingTop:5,
                       paddingLeft:17
                     }}
                   >
                   Nom de la société
                   </Text>
             </View>

          <View style={{flex:3,flexDirection:'column'}}>
            <View style={{flex:1}}></View>

                  <View
                    style={styles.container}
                     >
                   <View style={{paddingTop:10,paddingLeft:15}}>
                     <Image source={profileLogo}
                       style={styles.imageStyle}
                      />
                  </View>
                  <View style={{paddingTop:10,paddingLeft:24}}>
                    <Text
                      style={styles.fixTextStyle}
                      > Nom Complet</Text>
                  </View>
                  <View style={{paddingTop:10,paddingLeft:30}}>

                      <Text
                        style={styles.variableTextStyle}>
                        Hamza Houadria</Text>
                    </View>
                   </View>

                   <View
                     style={styles.container}>
                    <View style={{paddingTop:10,paddingLeft:15}}>
                      <Image source={mail}
                        style={styles.imageStyle}
                       />
                   </View>
                   <View style={{paddingTop:10,paddingLeft:30}}>
                     <Text
                       style={styles.fixTextStyle}
                       >Adresse Email</Text>
                   </View>
                   <View style={{paddingTop:10,paddingLeft:30}}>

                       <Text
                         style={styles.variableTextStyle}
                         >hamza.houadri...</Text>
                     </View>
                    </View>
                    <View
                      style={styles.container}>
                     <View style={{paddingTop:10,paddingLeft:15}}>
                       <Image source={phone}
                         style={styles.imageStyle}
                        />
                    </View>
                    <View style={{paddingTop:10,paddingLeft:30}}>
                      <Text
                        style={styles.fixTextStyle}
                        >Téléphone</Text>
                    </View>
                    <View style={{paddingTop:10,paddingLeft:55}}>

                        <Text
                          style={styles.variableTextStyle}
                          > +216 40 229 317</Text>
                      </View>
                     </View>
                    <View
                       style={styles.container}>
                      <View style={{paddingTop:10,paddingLeft:15}}>
                        <Image source={password}
                          style={styles.imageStyle}
                         />
                     </View>
                     <View style={{paddingTop:10,paddingLeft:32}}>
                       <Text
                         style={styles.fixTextStyle}
                         >Mot de passe</Text>
                     </View>
                      </View>
              <View style={{flex:2}}></View>



          </View>
          </View>
          </View>

    );
  }
}

function bindActions(dispatch) {
  return {
    fetchTripProfileAsync: jwtAccessToken =>
      dispatch(fetchTripProfileAsync(jwtAccessToken))
  };
}

export default connect(mapStateToProps, bindActions)(profile);
