import React, { Component } from 'react';
import FitImage from 'react-native-fit-image';
import { Actions} from "react-native-router-flux";
import ResponsiveImage from 'react-native-responsive-image';
import {
  Text,
  View,
  StyleSheet,
  Image,
  Platform,
  TouchableHighlight,
  TouchableOpacity
} from 'react-native';
import CommonStyles from "./CommonStyle.js";
import LinearGradient from 'react-native-linear-gradient';

export default class MenuItemBox extends Component {


  constructor(props) {
    super(props);

  }

  render() {
      return (
          <LinearGradient
              start={{x: 0.2, y: 0.2}} end={{x: 1.0, y: 2.0}}
              colors={['#D84315', '#FB8C00', '#FFD54F']}
              style={styles.boxMain}>
              <TouchableOpacity
                  underlayColor={'rgb(32, 158, 155)'}
                  style={styles.highLightBoxMain}
                  onPress={this.props.onPressBoxItem}>
                  <View style={styles.content}>
                      <Image
                          source={this.props.boxIcon}
                          style={[styles.boxIcon, {width: this.props.boxIconWidth, height: this.props.boxIconHeight}]}
                      />
                      <Text style={[
                          CommonStyles.extraBold,
                          CommonStyles.itemHeaderText,
                          CommonStyles.whiteColor,
                          {textAlign: "center", fontSize:20}
                      ]}>
                          {this.props.boxTitle}
                      </Text>
                      <Text style={[
                          CommonStyles.whiteColor,
                          CommonStyles.regularBold,
                          {fontSize: 14, opacity: 0.7, top: -5}
                      ]}>
                          {this.props.boxSubTitle}
                      </Text>
                  </View>
              </TouchableOpacity>
          </LinearGradient>
      );
  }
}

const styles = StyleSheet.create({
    boxMain: {
        height: 130,
        borderWidth: 0,
        borderRadius: 8,
        marginTop: 15,
        marginBottom: 1,
        elevation: 12,
        alignItems: "center",
        justifyContent: "center"
    },
    highLightBoxMain: {
        height: 135,
        backgroundColor: 'transparent',
        borderWidth: 0,
        borderRadius: 9,
        paddingTop: 20,
        paddingLeft: 15,
        paddingRight: 15,
        alignItems: "center",
        justifyContent: "center"
    },
    boxIcon: {
        marginBottom: 5,
    },
    content: {
        alignItems: "center",
        justifyContent: "center",
        height:200,
        width:130
    }
});

//const PropTypes = React.PropTypes;
/*
MenuItemBox.propTypes = {
  boxTitle: PropTypes.string,
  boxSubTitle: PropTypes.string,
  onPressBoxItem: PropTypes.func,
  boxIcon: PropTypes.number,
  boxIconHeight: PropTypes.number,
  boxIconWidth: PropTypes.number,
};
*/
