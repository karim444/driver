import React, { Component } from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity, Dimensions, Switch  } from 'react-native'
import Modal from "react-native-modal";
import PropTypes from 'prop-types';

let profil = require('../../../../assets/images/avatar-client.png')

const { width, height } = Dimensions.get("window");

export default class DriverModal extends Component {
  constructor(props) {
  super(props);
  this.state = {
    isVisible: props.isVisible,
    i:1
  };

}
hideModal = () => {
  this.setState({
    isVisible: false,
  });
}
  render() {
    if(this.props.isVisible && this.state.i==1){
      this.setState({isVisible:true,i:2})
    }
    return (
      <Modal isVisible={this.state.isVisible} onBackdropPress={()=>this.hideModal()}>
        <View style={styles.modalContainer}>
          <Image
            source={profil}
            style={styles.logo} />
        <View style={{top:20,flexDirection:'column',justifyContent:'space-around'}}>
          <Text style={styles.textOne}>Bonjour John Donavan,</Text>
          <View style={{top:10}}>
          <Text style={styles.textTwo}>Etes-vous disponibles pour prendre des courses?</Text>
          </View>
          <View style={{top:40}}>
          <Text style={styles.textThree}>veuillez confirmer votre disponibilité</Text>
        </View>
        </View>
        <View style={{paddingTop:70}}>
        <Switch
          onValueChange={value => this.setState({ isHidden: value })}
          value={this.state.isHidden}
         />
        </View>

        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  modalContainer: {
    height: '50%',
    borderRadius: 30,
    width: width * 0.8,
    backgroundColor: 'white',
    alignItems: 'center',
    marginLeft:15
  },

  logo: {
    height: 80,
    width : 80,
    borderRadius: 40,
    top: 15,
  },

  text: {
    textAlign: 'center',
  },
  textOne:{
    textAlign:'center',
    fontSize:16,
    color:'black',

  },
  textTwo:{
    textAlign:'center',
    fontSize:14,
    color:'black'

  },
  textThree:{
    textAlign:'center',
    fontSize:10,

  },
})
