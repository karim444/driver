import React, { Component } from "react";
import { connect } from "react-redux";
import { Platform, Dimensions, Image , TouchableOpacity} from "react-native";
import PropTypes from "prop-types";
import {
  Content,
  View,
  Text,
  Icon,
  Card,
  CardItem,
  Thumbnail,
  Item,
  List,
  ListItem,
  Left
} from "native-base";
import _ from "lodash";
import { Actions, ActionConst } from "react-native-router-flux";
import { closeDrawer } from "../../../actions/drawer";
import { logOutUserAsync } from "../../../actions/common/signin";
import LinearGradient from 'react-native-linear-gradient';


import styles from "./styles";
import commonColor from "../../../../native-base-theme/variables/commonColor";
const deviceHeight = Dimensions.get("window").height;

let home=require('../../../../assets/images/home1.png')
let communication=require('../../../../assets/images/communication.png')
let historique=require('../../../../assets/images/historique1.png')
let paiement=require('../../../../assets/images/paiment.png')
let gear=require('../../../../assets/images/gear.png')
let courses=require('../../../../assets/images/cars.png')
let profile=require('../../../../assets/images/avatar-client.png')





function mapStateToProps(state) {
  return {
    fname: state.driver.user.fname,
    lname: state.driver.user.lname,
    email: state.driver.user.email,
    profileUrl: state.driver.user.profileUrl,
    token: state.driver.appState.jwtAccessToken
  };
}
class SideBar extends Component {
  static propTypes = {
    fname: PropTypes.string,
    closeDrawer: PropTypes.func,
    logOutUserAsync: PropTypes.func
  };
  constructor(props) {
    super(props);
    this.state = {
      image: null
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.fname === undefined) {
      Actions.login({ type: ActionConst.RESET });
    }
  }

  handleLogOut() {
    this.props.logOutUserAsync(this.props.token);
  }

  render() {
    return (
      <LinearGradient colors={["#FB8C00","#FF6D00","#FF3D00", "#BF360C"]} style={styles.linearGradient}>

      <View style={{ flex: 1 }}>

        <Content
          bounces={false}
          scrollEnabled={false}
          showsVerticalScrollIndicator={false}
          style={
            Platform.OS === "android"
              ? styles.adrawerContent
              : styles.drawerContent
          }
        >

          <Card
            style={styles.cardStyle}>
            <CardItem
              style={styles.cardItemStyle}>
              <Image
                source={profile}
                style={styles.imageProfilStyle}

              />


              {/* </Item> */}


              <Text
                style={styles.textStyle}>

                {_.get(this.props, "fname", "")}{" "}
                {_.get(this.props, "lname", "")}
              </Text>
              <View style={{
                  flex:1,
                  flexDirection:'row',
                  alignItems:'center',
                  paddingRight:35,

                }}
                >
              <Text
                note
                style={styles.textMailStyle}>
                {_.get(this.props, "email", "")}
              </Text>
              <TouchableOpacity
                button
                onPress={() => {
                Actions.profile();
                this.props.closeDrawer();
                         }}
              >
                <Image
                source={gear}
                style={styles.gearStyle}

                />
            </TouchableOpacity>
            </View>

            </CardItem>
          </Card>
          <List foregroundColor={"white"} style={styles.Bg}>
            <View style={{paddingTop:40}}>
            <ListItem
              button
              onPress={() => {
                this.props.closeDrawer();
              }}
              iconLeft
              style={Platform.OS === "android" ? styles.alinks : styles.links}
            >
              <Left>
                <Image
                  source={home}
                  style={styles.iconStyle}
                />
              <Text style={styles.linkText}>Accueil</Text>
              </Left>
            </ListItem>
          </View>
            <ListItem
              button
               onPress={() => {
                 Actions.aVenir();
                 this.props.closeDrawer();
               }}
              iconLeft
              style={Platform.OS === "android" ? styles.alinks : styles.links}
            >
              <Left>
                <Image
                  source={courses}
                  style={styles.iconStyle}
                />
              <Text style={styles.linkText}>Mes Courses</Text>
              </Left>
            </ListItem>

            <ListItem
              button
              onPress={() => {
                Actions.history();
                this.props.closeDrawer();
              }}
              iconLeft
              style={Platform.OS === "android" ? styles.alinks : styles.links}
            >
              <Left>
                <Image
                  source={historique}
                  style={styles.iconStyle}
                />
                <Text style={styles.linkText}>
                  Historique
                </Text>
              </Left>
            </ListItem>

            <ListItem
              button
              onPress={() => {
                Actions.ContactUs();
                this.props.closeDrawer();
              }}
              iconLeft
              style={Platform.OS === "android" ? styles.alinks : styles.links}
            >
              <Left>

              <Text style={styles.centerText1}>Nous contacter</Text>
              </Left>
            </ListItem>

            <ListItem
              button
              onPress={() => {
                Actions.history();
                this.props.closeDrawer();
              }}
              iconLeft
              style={Platform.OS === "android" ? styles.alinks : styles.links}
            >
              <Left>

              <Text style={styles.centerText2}>Mentions Légales</Text>
              </Left>
            </ListItem>
            <View style={{paddingTop:20}}>
            <ListItem
              button
              onPress={() => {
                 Actions.login({ type: ActionConst.RESET });
                 this.props.closeDrawer();
                 this.handleLogOut();
               }}
              iconLeft
              style={Platform.OS === "android" ? styles.alinks : styles.links}
            >
              <Left>

              <Text style={styles.disconnectText}>Se Déconnecter</Text>
              </Left>
            </ListItem>
          </View>
          </List>
        </Content>
      </View>
    </LinearGradient>




    );
  }
}

function bindAction(dispatch) {
  return {
    closeDrawer: () => dispatch(closeDrawer()),
    logOutUserAsync: token => dispatch(logOutUserAsync(token))
  };
}

export default connect(
  mapStateToProps,
  bindAction
)(SideBar);
