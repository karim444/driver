import React, { Component } from 'react'
import { Text,Button, View, Image, StyleSheet, TouchableOpacity, Dimensions, Switch,ScrollView ,TextInput } from 'react-native'
import Modal from "react-native-modal";
import PropTypes from 'prop-types';

let profil = require('../../../../assets/images/avatar-client.png')

const { width, height } = Dimensions.get("window");

export default class FormulaireModal extends Component {
  constructor(props) {
  super(props);
  this.state = {
    isVisible: props.isVisible,
    i:1,
    TextInputValueHolder: '',
  };
  GetValueFunction = () =>{

 const { TextInputValueHolder }  = this.state ;

    Alert.alert(TextInputValueHolder)

  }

}
hideModal = () => {
  this.setState({
    isVisible: false,
  });
}
  render() {
    if(this.props.isVisible && this.state.i==1){
      this.setState({isVisible:true,i:2})
    }
    return (

      <Modal isVisible={this.state.isVisible} onBackdropPress={()=>this.hideModal()}>
        <View style={styles.modalContainer}>


          <View style={{flexDirection:'column'}}>

            <View style={{top:10}}>
            <Text style={styles.textOne}>Réception des bagages</Text>
            </View>

            <View style={{top:40,left:20}}>
            <Text style={styles.textTwo}>lieu de dépôt</Text>
            </View>
            <View style={{top:45,left:20}}>
            <Text style={styles.textThree}>Hotel Radisson</Text>
          </View>

          <View style={{top:60,left:20}}>
          <Text style={styles.textTwo}>Adresse</Text>
          </View>
          <View style={{top:65,left:20}}>
          <Text style={styles.textThree}>Rue d'Orsel, Paris 75025</Text>
        </View>

        <View style={{top:80,left:20}}>
        <Text style={styles.textTwo}>Nom du réceptionniste</Text>
        </View>
        <View style={{top:85,left:20}}>
          <TextInput
           placeholder="Nom"
           placeholderTextColor="#BDBDBD"
           style={{width:250,fontSize:12,color:'#424242'}}
             />

      </View>
      <View style={{top:100,left:20}}>
      <Text style={styles.textTwo}>Prénom du réceptionniste</Text>
      </View>
      <View style={{top:105,left:20}}>
        <TextInput
         placeholder="Prénom"
         placeholderTextColor="#BDBDBD"
         style={{width:250,fontSize:12,color:'#424242'}}
           />
      </View>
      <View style={{top:120,left:20}}>
      <Text style={styles.textTwo}>Code Bagage</Text>
      </View>
      <View style={{top:125,left:20}}>
        <TextInput
         placeholder="XXXXXX"
         placeholderTextColor="#BDBDBD"
         style={{width:250,fontSize:12,color:'#424242'}}
           />
      </View>
          </View>
          <View style={{top:160,paddingLeft:20,paddingRight:20}}>
          <Button title="TERMINER" onPress={this.GetValueFunction} color="#FF6F00" />
          </View>
        </View>


      </Modal>

    )
  }
}

const styles = StyleSheet.create({


  modalContainer: {
    height:'90%',
    width:'90%',
    margin:15,
    backgroundColor: "#fff",
    borderRadius: 20,
    flexDirection:'column',
    flex:1,

  },

  textStyle: {
    textAlign: 'center',
    fontSize:16,
    color:'#1565C0',
    fontWeight:'bold'
  },
  textOne:{
    textAlign:'center',
    fontSize:15,
    color:'#1565C0',
    fontWeight:'bold',
    textAlign:'center',
  },
  textTwo:{
    textAlign:'left',
    fontSize:15,
    color:'#757575',
    fontWeight:'bold'
  },
  textThree:{
    textAlign:'left',
    fontSize:12,
    color:'#757575',
  },


})
