import commonColor from '../../../../native-base-theme/variables/commonColor';
const React = require('react-native');

const {Dimensions} = React;

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default {

  pageTouch: {
    backgroundColor: 'transparent',
    position: 'absolute',     shadowColor: '#000',

    top: 0,
    right: 0,
    left: 0,
    height: deviceHeight,
    width: deviceWidth,
    flex: 999999,
  },
  container: {
    flex: 1,
    position: 'relative',
    backgroundColor: commonColor.brandPrimary,
  },
  detailsContainer: {
    padding: 30,
    paddingTop: 40,
    alignItems: 'center',
    position: 'absolute',
    top: deviceHeight / 2,
    right: 0,
    left: 0,
  },
  time: {
    color: '#fff',
    fontWeight: '700',
    fontSize: 20,
    textAlign: 'center',
    padding: 10,
  },
  place: {
    fontWeight: '500',
    fontSize: 16,
    textAlign: 'center',
    padding: 10,
    opacity: 0.8,
  },
  rating: {
    color: '#ccc',
    fontWeight: '500',
    fontSize: 14,
    textAlign: 'center',
    paddingVertical: 10,
  },
  iosRateStar: {
    marginTop: 5,
    fontSize: 16,
    color: '#ccc',
    alignSelf: 'center',
    textAlign:'center'
  },
  aRateStar: {
    marginTop: -23,
    fontSize: 16,
    color: '#ccc',
    alignSelf: 'center',
  },
  iosHeader: {
    //alignSelf: "stretch",
    width:'100%',
     backgroundColor: "transparent",
     borderColor: "#aaa",
     //elevation: 3,
     alignItems: "center",
     display:'flex',
     justifyContent:'center',
     alignItems:'center',
     height:30,
     paddingTop:20,
     paddingBottom:20,

   },
 }
