import React, { Component } from "react";
import { connect } from "react-redux";
import {
  View,
  Platform,
  TouchableOpacity,
  BackHandler,
  NetInfo,
  Image,
  ScrollView
} from "react-native";
import PropTypes from "prop-types";
import {
  Content,
  Text,
  Header,
  Button,
  Icon,
  Title,
  Left,
  Right,
  Body,
  Switch,
  Container
} from "native-base";
import { Actions, ActionConst } from "react-native-router-flux";
import LinearGradient from 'react-native-linear-gradient';
import PremiumBox from "./menuItems.js";
import AllInBox from "./allIn.js";

import { openDrawer, closeDrawer } from "../../../actions/drawer";
import {
  changePageStatus,
  currentLocationDriver
} from "../../../actions/driver/home";
import {
  updateUserProfileAsync,
  updateAvailable
} from "../../../actions/driver/settings";
import { connectionState } from "../../../actions/network";
import { logOutUserAsync } from "../../../actions/common/signin";
import styles from "./styles";
import commonColor from "../../../../native-base-theme/variables/commonColor";

let monProfil=require('../../../../assets/images/monprofil.png')
let historique=require('../../../../assets/images/historiques.png')
let avenir=require('../../../../assets/images/avenir.png')
let encours=require('../../../../assets/images/encours.png')
let profil=require('../../../../assets/images/avatar-client.png')


function mapStateToProps(state) {
  return {
    tripRequest: state.driver.tripRequest,
    fname: state.driver.user.fname,
    jwtAccessToken: state.driver.appState.jwtAccessToken,
    userDetails: state.driver.user,
    isAvailable: state.driver.user.isAvailable
  };
}
class aVenir extends Component {
  static propTypes = {
    logOutUserAsync: PropTypes.func,
    jwtAccessToken: PropTypes.string,
    openDrawer: PropTypes.func,
    currentLocationDriver: PropTypes.func
  };
  constructor(props) {
    super(props);
    this.state = {
      flag: true,
      switchValue: this.props.isAvailable
    };
  }
  componentWillMount() {
    this.props.closeDrawer();
  }

  componentDidMount() {
    NetInfo.isConnected.addEventListener(
      "change",
      this._handleConnectionChange
    );
    BackHandler.addEventListener("hardwareBackPress", () => this.backAndroid()); // Listen for the hardware back button on Android to be pressed
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
      "change",
      this._handleConnectionChange
    );
    BackHandler.removeEventListener("hardwareBackPress", () =>
      this.backAndroid()
    ); // Remove listener
  }

  _handleConnectionChange = isConnected => {
    this.props.connectionState({ status: isConnected });
  };
  backAndroid() {
    Actions.pop(); // Return to previous screen
    return true; // Needed so BackHandler knows that you are overriding the default action and that it should not close the app
  }
  handleLogOut() {
    this.props.logOutUserAsync(this.props.jwtAccessToken);
  }
  driverAvailable(value) {
    let userData = Object.assign(this.props.userDetails, {
      isAvailable: value
    });
    this.setState({
      switchValue: value
    });
    this.props.updateAvailable(userData);
  }

  render() {
    // eslint-disable-line class-methods-use-this
    return (
      <View style={styles.headerContainer} pointerEvents="box-none">
      <ScrollView>

      <View style={{paddingTop:10,display:'flex',justifyContent:'space-around'}}>
     <View style={{flexDirection:'column', justifyContent:'space-around',alignItems:'center'}}>
       <AllInBox
         boxTitle={'Hamza Houadria'}
         boxSubTitle={'Safety TR'}
         boxIcon={require('../../../../assets/images/avatar-client.png')}
         boxIconWidth={60}
         boxIconHeight={60}
         phoneBox={require('../../../../assets/images/communication-icon.png')}
         phoneBoxWidth={40}
         phoneBoxHeight={40}
         msgBox={require('../../../../assets/images/telephone-icon.png')}
         msgBoxWidth={40}
         msgBoxHeight={40}
         gearBox={require('../../../../assets/images/offer.png')}
         gearBoxWidth={20}
         gearBoxHeight={20}
         offreBox={'Offre:'}
         offre={'Stockage'}
         separation={require('../../../../assets/images/divider.png')}
         separationWidth={2}
         separationHeight={100}
         mapBox={require('../../../../assets/images/map.png')}
         mapBoxWidth={25}
         mapBoxHeight={25}
         dateBox={require('../../../../assets/images/date.png')}
         dateBoxWidth={20}
         dateBoxHeight={20}
         date={'Date:'}
         currentDateBox={'5 Nov 2018 17:44'}
         adrBox={require('../../../../assets/images/adr.png')}
         adrBoxWidth={20}
         adrBoxHeight={20}
         adr={'Rue d Orsel, Paris 75401'}
         valiseBox={require('../../../../assets/images/bag-icon.png')}
         valiseBoxWidth={25}
         valiseBoxHeight={20}
         nbrValisesBox={4}
         valisesTxtBox={'Valises'}
         sandBox={require('../../../../assets/images/duree.png')}
         sandBoxWidth={15}
         sandBoxHeight={25}
         durationBox={'Durée estimée'}
         duration={'44min'}
         destBox={require('../../../../assets/images/adrY.png')}
         destBoxWidth={20}
         destBoxHeight={20}
         dest={'Hotel the Madison, Paris 75401'}


         />
       <View style={{top:130}}>
         <AllInBox
           boxTitle={'Hamza Houadria'}
           boxSubTitle={'Safety TR'}
           boxIcon={require('../../../../assets/images/avatar-client.png')}
           boxIconWidth={60}
           boxIconHeight={60}
           phoneBox={require('../../../../assets/images/communication-icon.png')}
           phoneBoxWidth={40}
           phoneBoxHeight={40}
           msgBox={require('../../../../assets/images/telephone-icon.png')}
           msgBoxWidth={40}
           msgBoxHeight={40}
           gearBox={require('../../../../assets/images/offer.png')}
           gearBoxWidth={20}
           gearBoxHeight={20}
           offreBox={'Offre:'}
           offre={'Stockage'}
           separation={require('../../../../assets/images/divider.png')}
           separationWidth={2}
           separationHeight={100}
           mapBox={require('../../../../assets/images/map.png')}
           mapBoxWidth={25}
           mapBoxHeight={25}
           dateBox={require('../../../../assets/images/date.png')}
           dateBoxWidth={20}
           dateBoxHeight={20}
           date={'Date:'}
           currentDateBox={'5 Nov 2018 17:44'}
           adrBox={require('../../../../assets/images/adr.png')}
           adrBoxWidth={20}
           adrBoxHeight={20}
           adr={'Rue d Orsel, Paris 75401'}
           valiseBox={require('../../../../assets/images/bag-icon.png')}
           valiseBoxWidth={25}
           valiseBoxHeight={20}
           nbrValisesBox={4}
           valisesTxtBox={'Valises'}
           sandBox={require('../../../../assets/images/duree.png')}
           sandBoxWidth={15}
           sandBoxHeight={25}
           durationBox={'Durée estimée'}
           duration={'44min'}
           destBox={require('../../../../assets/images/adrY.png')}
           destBoxWidth={20}
           destBoxHeight={20}
           dest={'Hotel the Madison, Paris 75401'}


           />
       </View>
     </View>
     </View>
   </ScrollView>

      </View>
    );
  }
}
function bindActions(dispatch) {
  return {
    closeDrawer: () => dispatch(closeDrawer()),
    openDrawer: () => dispatch(openDrawer()),
    changePageStatus: newPage => dispatch(changePageStatus(newPage)),
    logOutUserAsync: jwtAccessToken =>
      dispatch(logOutUserAsync(jwtAccessToken)),
    currentLocationDriver: () => dispatch(currentLocationDriver()),
    updateAvailable: userDetails => dispatch(updateAvailable(userDetails)),
    connectionState: status => dispatch(connectionState(status))
  };
}
export default connect(
  mapStateToProps,
  bindActions
)(aVenir);
