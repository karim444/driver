import React, { Component } from 'react';
import FitImage from 'react-native-fit-image';
import { Actions} from "react-native-router-flux";
import ResponsiveImage from 'react-native-responsive-image';
import {
  Text,
  View,
  StyleSheet,
  Image,
  Platform,
  TouchableHighlight,
  TouchableOpacity,
  Button
} from 'react-native';
import CommonStyles from "./CommonStyle.js";
import LinearGradient from 'react-native-linear-gradient';

export default class AllIn extends Component {


  constructor(props) {
    super(props);

  }

  render() {
      return (
        <View style={styles.containerStyle}>
          <View style={styles.boxMain}>
                    <View style={styles.firstBox}>
                      <View style={{paddingLeft:10}}>
                      <Image
                          source={this.props.boxIcon}
                          style={[styles.boxIcon, {width: this.props.boxIconWidth, height: this.props.boxIconHeight,borderRadius:30}]}
                      />
                  </View>
                  <View style={{flexDirection:'column',paddingLeft:10}}>
                      <Text style={[
                          CommonStyles.extraBold,
                          CommonStyles.itemHeaderText,
                          CommonStyles.blackColor,
                          {textAlign: "center", fontSize:13}
                      ]}>
                          {this.props.boxTitle}
                      </Text>
                      <View style={{paddingTop:5}}>
                      <Text style={[
                          CommonStyles.greyColor,
                          CommonStyles.regularBold,
                          {fontSize: 12, opacity: 0.7, top: -5}
                      ]}>
                          {this.props.boxSubTitle}
                      </Text>
                    </View>
                   </View>

                    <View style={{flexDirection:'row',paddingLeft:20}}>

                  <TouchableOpacity>
                      <Image
                          source={this.props.msgBox}
                          style={[styles.msgBox, {width: this.props.msgBoxWidth, height: this.props.msgBoxHeight,borderRadius:20}]}
                      />
                  </TouchableOpacity>
                   <View style={{paddingLeft:15}}>
                    <TouchableOpacity>
                    <Image
                        source={this.props.phoneBox}
                        style={[styles.phoneBox, {width: this.props.phoneBoxWidth, height: this.props.phoneBoxHeight,borderRadius:20}]}
                    />
                </TouchableOpacity>
              </View>
            </View>
          </View>

                    <View style={styles.secondBox}>
                    <View style={{paddingLeft:10}}>
                      <Image
                          source={this.props.gearBox}
                          style={[styles.gearBox, {width: this.props.gearBoxWidth, height: this.props.gearBoxHeight,borderRadius:10}]}
                      />
                  </View>
                  <View style={{paddingLeft:15}}>
                  <Text style={[
                      CommonStyles.extraBold,
                      CommonStyles.itemHeaderText,
                      CommonStyles.blackColor,
                      {textAlign: "center", fontSize:14}
                  ]}>
                      {this.props.offreBox}
                  </Text>
                </View>
                <View style={{paddingLeft:5}}>
                <Text style={[
                    CommonStyles.greyColor,
                    CommonStyles.regularBold,
                    {fontSize: 12, opacity: 0.7}
                ]}>
                    {this.props.offre}
                </Text>
              </View>
              <View style={{paddingLeft:15}}>
              <Image
                  source={this.props.separation}
                  style={[styles.separation, {width: this.props.separationWidth, height: this.props.separationHeight}]}
              />
          </View>
          <View style={{paddingLeft:12}}>
            <Image
                source={this.props.dateBox}
                style={[styles.dateBox, {width: this.props.dateBoxWidth, height: this.props.dateBoxHeight,resizeMode:'contain'}]}
            />
        </View>
        <View style={{paddingLeft:5}}>
        <Text style={[
            CommonStyles.extraBold,
            CommonStyles.itemHeaderText,
            CommonStyles.blackColor,
            {textAlign: "center", fontSize:12}
        ]}>
            {this.props.date}
        </Text>
      </View>
      <View style={{paddingLeft:1}}>
      <Text style={[
          CommonStyles.greyColor,
          CommonStyles.regularBold,
          {fontSize: 10, opacity: 0.7}
      ]}>
          {this.props.currentDateBox}
      </Text>
    </View>
  </View>
                    <View style={styles.thirdBox}>

                      <View style={{paddingLeft:10}}>
                        <Image
                            source={this.props.adrBox}
                            style={[styles.adrBox, {width: this.props.adrBoxWidth, height: this.props.adrBoxHeight,resizeMode:'contain'}]}
                        />
                    </View>
                    <View style={{paddingLeft:25}}>
                    <Text style={[
                        CommonStyles.extraBold,
                        CommonStyles.itemHeaderText,
                        CommonStyles.blackColor,
                        {textAlign: "center", fontSize:12}
                    ]}>
                        {this.props.adr}
                    </Text>
                  </View>
                  <View style={{paddingLeft:45}}>
                    <Image
                        source={this.props.mapBox}
                        style={[styles.mapBox, {left:30,width: this.props.mapBoxWidth, height: this.props.mapBoxHeight,resizeMode:'contain'}]}
                    />

                </View>
              </View>

                <View style={styles.destBox}>

                  <View style={{paddingLeft:10}}>
                    <Image
                        source={this.props.destBox}
                        style={[styles.destBox, {width: this.props.destBoxWidth, height: this.props.destBoxHeight,resizeMode:'contain'}]}
                    />
                </View>
                <View style={{paddingLeft:25}}>
                <Text style={[
                    CommonStyles.extraBold,
                    CommonStyles.itemHeaderText,
                    CommonStyles.blackColor,
                    {textAlign: "center", fontSize:12}
                ]}>
                    {this.props.dest}
                </Text>
              </View>
            </View>


                    <View style={styles.fourthBox}>

                      <View style={{paddingLeft:10}}>
                        <Image
                            source={this.props.valiseBox}
                            style={[styles.valiseBox, {width: this.props.valiseBoxWidth, height: this.props.valiseBoxHeight,resizeMode:'contain'}]}
                        />
                    </View>
                    <View style={{paddingLeft:20}}>
                    <Text style={[
                        CommonStyles.extraBold,
                        CommonStyles.itemHeaderText,
                        CommonStyles.blackColor,
                        {textAlign: "center", fontSize:14,color:'red'}
                    ]}>
                        {this.props.nbrValisesBox}
                    </Text>
                    </View>

                    <View style={{paddingLeft:10}}>
                    <Text style={[
                        CommonStyles.extraBold,
                        CommonStyles.itemHeaderText,
                        CommonStyles.blackColor,
                        {textAlign: "center", fontSize:14}
                    ]}>
                        {this.props.valisesTxtBox}
                    </Text>
                    </View>
                    <View style={{paddingLeft:41}}>
                    <Image
                        source={this.props.separation}
                        style={[styles.separation, {width: this.props.separationWidth, height: this.props.separationHeight}]}
                    />
                    </View>
                    <View style={{flex:1,display:'flex',flexDirection:'row',alignItems:'center',justifyContent:'space-around',width:'90%'}}>
                    <View>
                      <Image
                          source={this.props.sandBox}
                          style={[styles.sandBox, {width: this.props.sandBoxWidth, height: this.props.sandBoxHeight,resizeMode:'contain'}]}
                      />
                  </View>
                  <View>
                  <Text style={[
                      CommonStyles.extraBold,
                      CommonStyles.itemHeaderText,
                      CommonStyles.blackColor,
                      {textAlign: "center", fontSize:12}
                  ]}>
                      {this.props.durationBox}
                  </Text>
                </View>
                <View>
                <Text style={[
                    CommonStyles.greyColor,
                    CommonStyles.regularBold,
                    {fontSize: 10, opacity: 0.7}
                ]}>
                    {this.props.duration}
                </Text>
              </View>
            </View>



                    </View>





                <View style={styles.fifthBox}>

                 <View style={{width:140,borderRadius:50}}>
                  <Button
                    title="Annuler"
                    color="#FF1744"
                  />
                {this.props.btnAnnuler}
                </View>
                <View style={{width:140,borderRadius:50}}>
                 <Button
                   title="Demarrer"
                   color="#00C853"
                 />
               {this.props.btnDemarrer}
               </View>
              </View>
            </View>
          </View>
      );
  }
}

const styles = StyleSheet.create({

  containerStyle:{
    marginTop:50,
    width:'95%',
    height:'90%'

  },
    boxMain: {
        elevation:12,
        alignItems: "center",
        justifyContent: "center",
    },
    firstBox:{
      height:'22%',
      width:'95%',
      borderWidth: 0,
      borderTopLeftRadius: 8,
      borderTopRightRadius: 8,
      backgroundColor:'white',
      flexDirection:'row',
      borderBottomWidth:2,
      borderBottomColor:'#EEEEEE',
      display:'flex',
      alignItems:'center',
    },
    secondBox:{
      height:'12%',
      width:'95%',
      backgroundColor:'white',
      flexDirection:'row',
      borderBottomWidth:2,
      borderBottomColor:'#EEEEEE',
      display:'flex',
      alignItems:'center',
    },
    thirdBox:{
      height:'12%',
      width:'95%',
      backgroundColor:'white',
      flexDirection:'row',
      borderBottomWidth:2,
      borderBottomColor:'#EEEEEE',
      display:'flex',
      alignItems:'center',

    },
    destBox:{
      height:'12%',
      width:'95%',
      backgroundColor:'white',
      flexDirection:'row',
      borderBottomWidth:2,
      borderBottomColor:'#EEEEEE',
      display:'flex',
      alignItems:'center',

    },
    fourthBox:{
      height:'12%',
      width:'95%',
      backgroundColor:'white',
      flexDirection:'row',
      borderBottomWidth:2,
      borderBottomColor:'#EEEEEE',
      display:'flex',
      alignItems:'center',
    },
    fifthBox:{
      height:'20%',
      width:'95%',
      backgroundColor:'white',
      flexDirection:'row',
      borderWidth: 0,
      borderBottomLeftRadius: 8,
      borderBottomRightRadius: 8,
      justifyContent:'space-around',
      display:'flex',
      alignItems:'center',
    },
    highLightBoxMain: {
        height: 135,
        backgroundColor: 'transparent',
        borderWidth: 0,
        borderRadius: 9,
        paddingTop: 20,
        paddingLeft: 15,
        paddingRight: 15,
        alignItems: "center",
        justifyContent: "center"
    },
    boxIcon: {
        marginBottom: 5,

    },

});

//const PropTypes = React.PropTypes;
/*
MenuItemBox.propTypes = {
  boxTitle: PropTypes.string,
  boxSubTitle: PropTypes.string,
  onPressBoxItem: PropTypes.func,
  boxIcon: PropTypes.number,
  boxIconHeight: PropTypes.number,
  boxIconWidth: PropTypes.number,
};
*/
