import commonColor from "../../../../native-base-theme/variables/commonColor";
import { Dimensions } from "react-native";
const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;

export default {
	iosHeader: {
		backgroundColor: "#fff"
	},
	aHeader: {
		backgroundColor: "#fff",
		borderColor: "#aaa",
		elevation: 3
	},
	iosHeaderTitle: {
		fontSize: 18,
		fontWeight: "500",
		color: commonColor.brandPrimary
	},
	aHeaderTitle: {
		fontSize: 18,
		fontWeight: "500",
		lineHeight: 26,
		marginTop: -5,
		color: commonColor.brandPrimary
	},
	orText: {
		textAlign: "center",
		fontWeight: "700",
		color: "#fff"
	},
	regBtnContain: {
		paddingVertical: 20,
		paddingHorizontal: 10,
	},
	regBtn: {
		//left: deviceWidth - 350 ,
	//	justifyContent: 'center',
	//	alignItems: 'center',
		height: 50,
		width:180,
		backgroundColor:"white",
		borderBottomWidth: 2,
		borderBottomColor:"#FF9800",
	},
	googleLeft: {
		flex: 1,
		height: 50,
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "#B6382C",
		borderBottomLeftRadius: 4,
		borderTopLeftRadius: 4
	},
	fbLeft: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
		height: 50,
		backgroundColor: "#233772",
		borderBottomLeftRadius: 4,
		borderTopLeftRadius: 4
	},
	linearGradient: {
		flex: 1,
		height:45,
		width: deviceWidth,
	}
};
