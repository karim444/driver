import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { Text, Dimensions, Image,TouchableOpacity } from "react-native";
import { Item, Input, Button, View, Icon, Spinner } from "native-base";
import PropTypes from "prop-types";
import { signinAsync, forgotMail } from "../../../actions/common/signin";
import LinearGradient from 'react-native-linear-gradient';

import styles from "./styles";
import commonColor from "../../../../native-base-theme/variables/commonColor";
var name= require("../../../../assets/images/mon-profil-nameIcon.png");
var lock= require("../../../../assets/images/mon-profil-mdp-icon.png");
var ship= require("../../../../assets/images/LOGO-CHAUFFEUR.png");



const deviceWidth = Dimensions.get("window").width;
const validate = values => {
  const errors = {};
  if (!values.email) {
    errors.email = "Email is Required";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = "Invalid email";
  } else if (isNaN(Number(values.phoneNo))) {
    errors.phoneNo = "Must be a number";
  } else if (!values.password) {
    errors.password = "Password is Required";
  } else if (!values.phoneNo) {
    errors.phoneNo = "Mobile Number is Required";
  } else if (!values.fname) {
    errors.fname = "First name is Required";
  } else if (!values.lname) {
    errors.lname = "Last name is Required";
  }
  if (!values.password) {
    errors.password = "Password is Required";
  }
  return errors;
};

export const input = props => {
  const { meta, input } = props;
  return (
    <View >
      <Item>
        {props.type === "email" ? (
          <Image source ={name}
            style={{ color: commonColor.lightThemePlaceholder,
                     paddingRight:10,
            }} />

        ) : (
          <Image source ={lock} style={{ color: commonColor.lightThemePlaceholder}} />

          )}
        <Input {...input} {...props} />
      </Item>

      {meta.touched &&
        meta.error && <Text style={{ color: "red" }}>{meta.error}</Text>}
    </View>
  );
};
input.propTypes = {
  input: PropTypes.object,
  meta: PropTypes.object
};

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false
    };
  }
  static propTypes = {
    dispatch: PropTypes.func,
    handleSubmit: PropTypes.func,
    forgotMail: PropTypes.func,
    isFetching: PropTypes.bool
  };
  submit(values) {
    this.props.dispatch(signinAsync(values));
  }
  forgotPassword(values) {
    this.props.dispatch(forgotMail());
  }
  render() {
  		return (
  			<View>
  			<View>
  				<View style={{flex:1,marginLeft:70}}>
  				<View style={styles.regBtn}>
  					<Button  style={styles.regBtn}>
  						<Text style={{color:"#FF9800", fontSize:18,paddingLeft:28 }}>CONNEXION </Text>
  					</Button>
  				</View>


  			</View>
       <View style={{paddingTop:20}}>
  				<View style={{ padding: 10,justifyContent:'space-around' }}>
  					<Field
  						component={input}
  						type="email"
  						name="email"
  						placeholder="   email@gmail.com"
  						placeholderTextColor="#424242"
  						keyboardType="email-address"
  						autoCapitalize="none"
  					/>
  				</View>
  				<View style={{ padding: 10}}>
  					<Field
  						component={input}
  						placeholder="   ******"
  						secureTextEntry
  						placeholderTextColor="#424242"
  						name="password"
  						autoCapitalize="none"
  					/>
  				</View>
  				<Button transparent style={{paddingLeft:80}} onPress={this.forgotPassword.bind(this)}>
  					<Text style={{ color: "#9E9E9E",paddingLeft:10 }}>Mot de passe oublié ?</Text>
  				</Button>
  			</View>
  			<TouchableOpacity onPress={this.props.handleSubmit(this.submit.bind(this))} block>
  				{this.props.isFetching ? (
  					<Spinner />
  				) : (
       <View style={{paddingTop:15,paddingLeft:20}}>
  			<LinearGradient colors={["#E65100","#F57C00","#FFB74D", "#FFE0B2"]}
  				style={{
  				 height:45,
  				 borderRadius:7,
  				 width: deviceWidth -80,
  			   justifyContent:'center',
  			   paddingTop:20}}>

  			  <View style={{paddingTop:15,paddingLeft:23}}>
  					<Text style={{fontSize:15, fontWeight:'bold',paddingBottom:35,paddingLeft:60,color:"white"}}>SE CONNECTER</Text>
  			 </View>
  		</LinearGradient>
  	      </View>
  					)}
       </TouchableOpacity>
  	  </View>

  			</View>
  		);
    }

  }
  export default reduxForm({
  	form: "login", // a unique name for this form
  	validate
  })(LoginForm);
