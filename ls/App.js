import React from "react";

import { Sentry } from "react-native-sentry";
import Setup from "./js/setup";


// Sentry.config('https://b6ed956360f24cfaaa9d6c3758f2175f:56b5e83a85d84430862b4beb099cba79@sentry.io/217553').install();


export default class App extends React.Component {
  render() {
    console.disableYellowBox = true;
    return <Setup />;
  }
}
