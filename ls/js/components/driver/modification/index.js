import React, { Component } from "react";
import moment from "moment";
import { connect } from "react-redux";
import _ from "lodash";
import {
   AppRegistry,
   StyleSheet,
   TextInput,
   View,
   Alert,
   Platform,
   Dimensions,
   Image,
   TouchableOpacity
       }
        from 'react-native';
import FAIcon from "react-native-vector-icons/FontAwesome";
import PropTypes from "prop-types";
import LinearGradient from 'react-native-linear-gradient';
import { Field, reduxForm } from "redux-form";



import {
  Container,
  Header,
  Content,
  Text,
  Button,
  Icon,
  Thumbnail,
  Card,
  CardItem,
  Title,
  Left,
  Right,
  Body,
  input
} from "native-base";
import { Actions } from "react-native-router-flux";
import styles from "./styles";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

import commonColor from "../../../../native-base-theme/variables/commonColor";

let profilePic=require('../../../../assets/images/avatar-client.png');
let profileLogo=require('../../../../assets/images/mon-profil-nameIco.png');
let mail=require('../../../../assets/images/msg.png');
let password=require('../../../../assets/images/lock.png');
let phone=require('../../../../assets/images/tell.png');
let pencil=require('../../../../assets/images/pencil.png');
let submit=require('../../../../assets/images/check-actif.png');




const { width } = Dimensions.get("window");

function mapStateToProps(state) {
  return {
    jwtAccessToken: state.driver.appState.jwtAccessToken,
    fname: state.driver.user.fname,
    lname: state.driver.user.lname,
  };
}

class modification extends Component {

  constructor(){
    super();
    this.state ={
      status:false,
      TextInputValueHolder: '',
      statusPass:false,
    }
  }
  GetValueFunction = () =>{

 const { TextInputValueHolder }  = this.state ;

    Alert.alert('le nouveau N° est:' + TextInputValueHolder)

  }

  ShowHideTextComponentView = () =>{
  this.setState({status: !this.state.status})
}

ShowHidePass = () =>{
this.setState({statusPass: !this.state.statusPass})
}


  static propTypes = {
    jwtAccessToken: PropTypes.string,
    fetchTripProfileAsync: PropTypes.func,
  };


  render() {
    return (
      <View style={{flex:1}}>
        <LinearGradient
          start={{x: 1.0, y: 2.0}} end={{x: 0.2, y: 0.2}}
          colors={['#FF3D00', '#FF7043', '#FFB74D']}
          style={{flex: 1,
           height:35,
           width: deviceWidth}}>

           <View style={{flex:1,flexDirection:'row'}}>
              <Title
                style={{ color:"white",
                   fontSize:16,
                   fontWeight:'lighter',
                   paddingLeft:140,
                   paddingTop:13
                  }}>
             Mon Compte
              </Title>
              <View style={{paddingLeft:35,paddingTop:14}}>
              <TouchableOpacity
                transparent onPress={this.GetValueFunction}>
                  <Image
                    source={submit}
                    style={{ color:"white",height:25,width:25,resizeMode:"contain",paddingTop:15, paddingLeft:130 }}
                    />
                </TouchableOpacity>
            </View>
           </View>
         </LinearGradient>




           <View style={{flex:12,backgroundColor:"#F5F5F5",flexDirection:'column'}}>
             <View style={{flex:2,backgroundColor:"#F5F5F5",alignItems:'center'}}>
              <View
                 style={{
                   paddingLeft:135,
                   paddingTop:30}}>
               <Image source={profilePic}
                 style={{
                   width: 110,
                   height: 110,
                   borderRadius: 55,
                   borderWidth: 0,
                   borderColor: "transparent",
                   marginRight:123,

                 }}
                 >
               </Image>
             </View>
               <Text
                   style={{
                     color:'black',
                     fontSize: 22,
                     paddingTop:15,
                     paddingLeft:20
                   }}
                 >
                 {_.get(this.props, "fname", "")}{" "}
                 {_.get(this.props, "lname", "")}
                 </Text>

                 <Text
                     style={{
                       color:'red',
                       fontSize: 18,
                       fontFamily: 'sans-serif-light',
                       paddingTop:5,
                       paddingLeft:17
                     }}
                   >
                   Nom de la société
                   </Text>
             </View>
          <View style={{flex:3,flexDirection:'column'}}>
            <View style={{flex:1}}></View>

              <TouchableOpacity
                style={{flex:1}}
                onPress={() => {
                this.ShowHideTextComponentView();
              }}
              >
                    <View
                      style={{
                        backgroundColor:"white",
                        flex:1,
                        flexDirection:"row",
                         height:50,
                         width:deviceWidth,
                         borderTopWidth:1,
                         borderTopColor:'#E0E0E0',
                         borderBottomColor:'#E0E0E0',
                         borderBottomWidth:1,
                         alignItems:'center'
                       }}>
                     <View style={{paddingLeft:15}}>
                       <Image source={phone}
                         style={{
                           height:25,
                           width:25,
                           resizeMode: 'contain',
                           paddingLeft:20,
                         }}
                        />
                    </View>
                    <View style={{paddingLeft:30}}>
                      <Text
                        style={{
                          color:"#4E342E",
                          fontSize:15,
                          fontWeight:"lighter",

                        }}
                        >Modification tell</Text>

                    </View>
                    <View style={{paddingLeft:30}}>

                        <Text
                          style={{
                            color:"black",
                            fontSize:15,
                            fontFamily: 'sans-serif-light'
                          }}
                          > +216 40 229 317</Text>
                      </View>
                     </View>
                   </TouchableOpacity>

                         {this.state.status?

                         <View
                           style={{
                             backgroundColor:"white",
                             flex:1,
                             flexDirection:"row",
                              height:50,
                              width:deviceWidth,
                              borderBottomColor:'#E0E0E0',
                              borderBottomWidth:1,
                              alignItems:'center',
                              justifyContent:'space-around'
                            }}>
                            <Text
                              style ={styles.NtextStyle}>Nouveau N° tell:
                            </Text>
                            <TextInput
                             placeholder="Entrez le nouveau N°"
                             onChangeText={TextInputValueHolder => this.setState({TextInputValueHolder})}
                             style={{textAlign: 'center',marginBottom: 7, height:40,width:160,fontSize:12,color:'#FF3D00'}}
                            />

                          </View>

                          :null}

              <TouchableOpacity
              style={{flex:1}}
              onPress={() => {
              this.ShowHidePass();
                                }}
                                >
                     <View
                       style={{
                         backgroundColor:"white",
                         flex:1,
                         flexDirection:"row",
                          height:50,
                          width:deviceWidth,
                          borderBottomColor:'#E0E0E0',
                          borderBottomWidth:1,
                          alignItems:'center'

                        }}>

                    <View style={{
                      flexDirection:"row",
                       height:50,
                       width:deviceWidth,
                       alignItems:'center'
                    }}>
                      <View style={{paddingLeft:15}}>
                        <Image source={password}
                          style={{
                            height:25,
                            width:25,
                            resizeMode: 'contain',
                            paddingLeft:20,
                          }}
                         />
                     </View>
                     <View style={{paddingLeft:30}}>
                       <Text
                         style={{
                           color:"#4E342E",
                           fontSize:15,
                           fontWeight:"lighter",

                         }}
                         >Modification password</Text>
                     </View>
                    </View>
                   </View>
                 </TouchableOpacity>

                 {this.state.statusPass?
                   <View
                     style={{
                        backgroundColor:"white",
                        flexDirection:"column",
                        height:150,
                        width:deviceWidth,
                        borderBottomColor:'#E0E0E0',
                        borderBottomWidth:1,
                        alignItems:'center',
                        justifyContent:'center'
                      }}>
                   <View style={{flexDirection:'row'}}>
                      <Text
                        style ={styles.ActuelTextStyle}>Mot de passe actuel:
                      </Text>
                      <TextInput
                       placeholder=""
                       style={{textAlign: 'center',marginBottom: 7, height:40,width:160,fontSize:12,color:'#FF3D00'}}
                      />
                    </View>
                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                      <Text
                        style ={styles.textStyle}>Nouveau mot de passe:
                      </Text>
                      <TextInput
                       placeholder=""
                       style={{textAlign: 'center',marginBottom: 7, height:40,width:160,fontSize:12,color:'#FF3D00'}}
                      />
                  </View>

                  <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                    <Text
                      style ={styles.ConfirmationTextStyle}>Confirmation:
                    </Text>
                    <TextInput
                     placeholder=""
                     style={{textAlign: 'center',marginBottom: 7, height:40,width:160,fontSize:12,color:'#FF3D00'}}
                    />
                </View>

              </View>
              :null}

              <View style={{flex:2}}></View>
          </View>
          </View>
          </View>

    );
  }
}

function bindActions(dispatch) {
  return {
    fetchTripModificationAsync: jwtAccessToken =>
      dispatch(fetchTripModificationAsync(jwtAccessToken))
  };
}

export default connect(mapStateToProps, bindActions)(modification);
