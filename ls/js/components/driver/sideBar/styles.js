import commonColor from "../../../../native-base-theme/variables/commonColor";
const React = require("react-native");
const { Dimensions } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
	links: {
		paddingTop: 15,
		paddingBottom: 15,
		marginLeft: 0,
		borderBottomWidth: 2,
		//borderBottomColor: "#245071"
	},
	cardStyle:{
		marginBottom: 0,
		marginLeft: 0,
		marginRight: 0,
		marginTop: 0,
		flexWrap: "nowrap",
		backgroundColor:"transparent",
		borderColor:"transparent",
	},
	cardItemStyle:{
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "center",
		height: deviceHeight / 4 + 10,
		marginRight:5,
		backgroundColor:"transparent",
		borderColor:"transparent",
		borderTopColor:"transparent",
		borderBottomColor:"transparent",
		paddingRight:100,
	},
	imageProfilStyle:{
		width: 90,
		height: 90,
		borderRadius: 45,
		borderWidth: 0,
		borderColor: "transparent",
		marginRight:123,
	},
	textStyle:{
		color: "#fff",
		fontSize: 20,
		fontWeight: "lighter",
		paddingVertical: 2,
		paddingRight:75,
	},
	textMailStyle:{
		 color: "#ddd",
		  paddingVertical: 5,
			 fontWeight: "400",
			 paddingRight:30,
			 paddingBottom:1,
	},
	iconStyle:{
		height:25,
		 width:25,
		 resizeMode: 'contain',
		 paddingLeft:70,
	},
	gearStyle:{
		height:16,
		width:16,
		borderRadius:8,
		paddingTop:15,
	},
	alinks: {
		paddingTop: 15,
		paddingBottom: 15,
		marginLeft: 0,
		borderBottomWidth: 2,
		borderBottomColor: "transparent"
	},
	iosAboutlink: {
		paddingTop: 10,
		paddingBottom: 10,
		paddingLeft: 20,
		borderBottomWidth: 0,
		borderTopWidth: 1,
		//borderTopColor: "#232232",
		borderBottomColor: "transparent"
	},
	aAboutlink: {
		paddingTop: 8,
		paddingBottom: 8,
		borderBottomColor: "transparent"
	},
	linkText: {
		color: "#fff",
		fontSize: 16,
		fontWeight: "400",
		paddingRight:200
	},
	logoutContainer: {
		padding: 30
	},
	logoutbtn: {
		paddingTop: 30,
		flexDirection: "row",
		borderTopWidth: 1,
		//borderTopColor: "#797979"
	},
	background: {
		flex: 1,
		width: null,
		height: null,
		//backgroundColor: "rgba(0,0,0,0.1)"
	},
	drawerContent: {
		paddingTop: 30,
		//backgroundColor: "#0A3A55"
	},
	Bg: {
		//backgroundColor: "#1565C0",
		height: deviceHeight - (deviceHeight / 5 - 30)
	},
	adrawerContent: {
		paddingTop: 20,
		//backgroundColor: "#0A3A55"
	},
	aProfilePic: {
		height: 40,
		width: 40,
		borderRadius: 40,
		marginLeft: 15,
		fontSize: 25
	},
	iosProfilePic: {
		height: 40,
		width: 40,
		borderRadius: 20,
		marginLeft: 5,
		fontSize: 25
	},
	aSidebarIcons: {
		marginLeft: 30,
		fontWeight: "600",
		color: "#fff",
		fontSize: 25,
		opacity: 0.8,
		width: 25
	},
	iosSidebarIcons: {
		color: "#fff",
		marginLeft: 30,
		fontWeight: "600",
		marginTop: 2,
		fontSize: 25,
		opacity: 0.8
	},
	profile: {
		//backgroundColor: "rgba(255,255,255,0.05)",
		paddingTop: 10,
		paddingBottom: 10
	},
	centerText1:{
		paddingTop:20,
		flex:1,
		color:"#BDBDBD",
		paddingLeft:75,
		fontSize: 16,
		fontWeight: "300"
	},
	centerText2:{
		flex:1,
		color:"#BDBDBD",
		paddingLeft:75,
		fontSize: 16,
		fontWeight: "300"
	},
	disconnectText:{
		flex:1,
		color:"white",
		paddingTop:20,
		paddingLeft:75,
		fontSize: 16,
		fontWeight: "500"
	},
	linearGradient: {
		flex: 1,
		height:45,
		width: deviceWidth,

	}
};
