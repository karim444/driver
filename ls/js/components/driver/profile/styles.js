import {
    StyleSheet,
    Dimensions,
} from 'react-native';

export const NAV_TAB_HEIGHT = 95;
export const NAV_HEIGHT = 45;
export const TAB_HEIGHT = 50;

export const deviceHeight = Dimensions.get('window').height;
export const deviceWidth = Dimensions.get('window').width;

export default styles = StyleSheet.create({

    container: {
       backgroundColor:"white",
       flex:1,
       flexDirection:"row",
       height:50,
       width:deviceWidth,
       borderBottomColor:'#E0E0E0',
       borderBottomWidth:1,
    },
    imageStyle: {
      height:25,
      width:25,
      resizeMode: 'contain',
      paddingLeft:20,
    },
    fixTextStyle: {
      color:"black",
      fontSize:18,
      fontFamily: 'sans-serif-light',
    },
    variableTextStyle: {
      color:"#757575",
      fontSize:18,
      fontFamily: 'sans-serif-light',
    },

});
