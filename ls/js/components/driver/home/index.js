import React, { Component } from "react";
import { connect } from "react-redux";
import {
  View,
  Platform,
  TouchableOpacity,
  BackHandler,
  NetInfo,
  Image
} from "react-native";
import PropTypes from "prop-types";
import {
  Content,
  Text,
  Header,
  Button,
  Icon,
  Title,
  Left,
  Right,
  Body,
  Switch,
  Container
} from "native-base";
import { Actions, ActionConst } from "react-native-router-flux";
import LinearGradient from 'react-native-linear-gradient';
import MenuItemBox from "./menuItems.js";

import { openDrawer, closeDrawer } from "../../../actions/drawer";
import {
  changePageStatus,
  currentLocationDriver
} from "../../../actions/driver/home";
import {
  updateUserProfileAsync,
  updateAvailable
} from "../../../actions/driver/settings";
import { connectionState } from "../../../actions/network";
import { logOutUserAsync } from "../../../actions/common/signin";
import styles from "./styles";
import commonColor from "../../../../native-base-theme/variables/commonColor";

let monProfil=require('../../../../assets/images/monprofil.png')
let historique=require('../../../../assets/images/historiques.png')
let avenir=require('../../../../assets/images/avenir.png')
let encours=require('../../../../assets/images/encours.png')
let profil=require('../../../../assets/images/avatar-client.png')


function mapStateToProps(state) {
  return {
    tripRequest: state.driver.tripRequest,
    fname: state.driver.user.fname,
    jwtAccessToken: state.driver.appState.jwtAccessToken,
    userDetails: state.driver.user,
    isAvailable: state.driver.user.isAvailable
  };
}
class DriverHome extends Component {
  static propTypes = {
    logOutUserAsync: PropTypes.func,
    jwtAccessToken: PropTypes.string,
    openDrawer: PropTypes.func,
    currentLocationDriver: PropTypes.func
  };
  constructor(props) {
    super(props);
    this.state = {
      flag: true,
      switchValue: this.props.isAvailable
    };
  }
  componentWillMount() {
    this.props.closeDrawer();
  }

  componentDidMount() {
    NetInfo.isConnected.addEventListener(
      "change",
      this._handleConnectionChange
    );
    BackHandler.addEventListener("hardwareBackPress", () => this.backAndroid()); // Listen for the hardware back button on Android to be pressed
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
      "change",
      this._handleConnectionChange
    );
    BackHandler.removeEventListener("hardwareBackPress", () =>
      this.backAndroid()
    ); // Remove listener
  }

  _handleConnectionChange = isConnected => {
    this.props.connectionState({ status: isConnected });
  };
  backAndroid() {
    Actions.pop(); // Return to previous screen
    return true; // Needed so BackHandler knows that you are overriding the default action and that it should not close the app
  }
  handleLogOut() {
    this.props.logOutUserAsync(this.props.jwtAccessToken);
  }
  driverAvailable(value) {
    let userData = Object.assign(this.props.userDetails, {
      isAvailable: value
    });
    this.setState({
      switchValue: value
    });
    this.props.updateAvailable(userData);
  }

  render() {
    // eslint-disable-line class-methods-use-this
    return (
      <View style={styles.headerContainer} pointerEvents="box-none">
        <LinearGradient
              start={{x: 0.2, y: 0.2}} end={{x: 1.0, y: 2.0}}
              colors={['#FF3D00', '#FF7043', '#FFB74D']}>
        <Header
          style={styles.iosHeader}
        >
          <View style={{position:'absolute',top:7,left:10}}>
            <Button transparent>
              <Icon
                name="ios-menu"
                style={styles.logoutLogo}
                onPress={this.props.openDrawer}
              />
            </Button>
          </View>
          <View>
            <Title
              style={styles.shipStyle} >
              SHIPOTSU
            </Title>
          </View>
        </Header>
      </LinearGradient>

      <View>
         <Text
           style={{top:10,fontSize:20,fontWeight:'lighter',display:'flex',justifyContent:'center',alignItems:'center',textAlign:'center',color:'black'}}>
           BONJOUR
         </Text>
         <View style={{display:'flex',justifyContent:'center',alignItems:'center',paddingTop:15}}>
         <Image
           source={profil}
           style={{
             height:140,
             width:140,
             borderRadius:70,
           }}/>
       </View>
       <Text
         style={{fontSize:15,display:'flex',justifyContent:'center',alignItems:'center',textAlign:'center',color:'black',top:10}}>
         John Donovan
       </Text>
       </View>

      <View style={{paddingTop:10}}>
      <View style={{display:'flex',flexDirection:'row', justifyContent:'space-around'}}>
       <MenuItemBox
         boxTitle={'A Venir'}
         boxIcon={require('../../../../assets/images/avenir.png')}
         boxIconWidth={70}
         boxIconHeight={60}
         onPressBoxItem={()=>{Actions.aVenir();
         }}/>
       <MenuItemBox
         boxTitle={'En Cours'}
         boxIcon={require('../../../../assets/images/encours.png')}
         boxIconWidth={55}
         boxIconHeight={70}
         onPressBoxItem={()=>{Actions.enCours();
         }}/>
     </View>
     <View style={{flexDirection:'row', justifyContent:'space-around'}}>
       <MenuItemBox
         boxTitle={'Historique'}
         boxIcon={require('../../../../assets/images/historiques.png')}
         boxIconWidth={55}
         boxIconHeight={70}
         onPressBoxItem={() => {
           Actions.history();
         }}
         />
       <MenuItemBox
         boxTitle={'Mon Profil'}
         boxIcon={require('../../../../assets/images/monprofil.png')}
         boxIconWidth={55}
         boxIconHeight={70}
         onPressBoxItem={()=>{Actions.profile();
         }}/>

     </View>
     </View>
      </View>
    );
  }
}
function bindActions(dispatch) {
  return {
    closeDrawer: () => dispatch(closeDrawer()),
    openDrawer: () => dispatch(openDrawer()),
    changePageStatus: newPage => dispatch(changePageStatus(newPage)),
    logOutUserAsync: jwtAccessToken =>
      dispatch(logOutUserAsync(jwtAccessToken)),
    currentLocationDriver: () => dispatch(currentLocationDriver()),
    updateAvailable: userDetails => dispatch(updateAvailable(userDetails)),
    connectionState: status => dispatch(connectionState(status))
  };
}
export default connect(
  mapStateToProps,
  bindActions
)(DriverHome);
