import { AppRegistry } from "react-native";
import { Sentry } from "react-native-sentry";
import App from "./App";

Sentry.config("YOUR_DNS").install();
AppRegistry.registerComponent("Driver", () => App);
