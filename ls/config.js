export default {
  OnesignalAppId: "YOUR_APP_ID",
  clientID: "GOOGLE_CLIENT_ID", // For Enabling Google Signin
  // local server
  serverSideUrl: "http://91.121.108.43", // use Ip address instead of localhost
  port: 3040 // incase of running api-server (node app) in development
  // port: 3066 //incase of running api-server (node app) in production
  //  port: 443 //incase of running api-server (node app) on heroku
};
